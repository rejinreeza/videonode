const express = require('express');
const https = require('https');
const { Pool } = require('pg')
const { ExpressPeerServer } = require('peer');
var fs = require('fs');

var sslOptions = {
//    key: fs.readFileSync('/opt/rej/myqpp/config/cert/example.key'),
  //  cert: fs.readFileSync('/opt/rej/myqpp/config/cert/example.crt')
  };

const pool = new Pool({
	host: "localhost",
    user: 'webrtc',
    database: 'myqpp_development',
    password: 'password'
})

const app = express();

app.get('/', (req, res, next) => res.send('Hello world!'));
const server = app.listen(9000);
//const server = https.createServer(sslOptions, app).listen(9000)

const peerServer = ExpressPeerServer(server, {
    debug: true,
    path: '/myapp'
  });
app.use('/', peerServer);
peerServer.on('connection', (client) => {
    const tok = client.getId();
	console.log('connected',tok);
    pool.connect((err, client, done) => {
        if (err) throw err
        
        client.query('update users set host_status=$1 WHERE rtc_id = $2', [true,tok], (err, res) => {
          done()
          if (err) {
            console.log(err.stack)
          } else {
           // console.log(res)
          }
        })
    })
});

peerServer.on('disconnect', (client) => {
    const tok = client.getId();
	console.log('disconnected',tok);
    pool.connect((err, client, done) => {
        if (err) throw err
        
        client.query('update users set host_status=$1 WHERE rtc_id = $2', [false,tok], (err, res) => {
          done()
          if (err) {
            console.log(err.stack)
          } else {
           // console.log(res)
          }
        })
    })
});
